#let fg0 = rgb("#D4D4E0");
#let fg1 = rgb("#F1F1FF");
#let accent0 = rgb("#C00000");
#let accent1 = rgb("#73140C");
#let backgroud = rgb("#BCBCBC")

// here you can configure the poster background; if you change it, don't forget to adjust refs.bib
#let background = "../images/background.jpg"

// here you can configure which SeeSat logo suits the background better
#let seesat_logo = "seesat-logo_light.svg"
// #let seesat_logo = "seesat-logo_dark.svg"

#let poster(
  title: "",
  authors: (),
  body
) = {
  set page(
    paper: "a0",
    margin: 0pt,
    background: image(width: 100%, height: 100%, background),
  )
  
  set text(
    font: ("Roboto Serif", "New Computer Modern"),
    size: 30pt,
    fill: fg0,
    lang: "en",
    region: "us",
  )
  
  block(
    inset: 1cm,
    grid(
      columns: (1fr, auto, auto),
      gutter: 1cm,
      stack(
        dir: ttb,
        spacing: 2cm,
        text(size: 80pt, fill: accent0, strong(title)),
        text(size: 60pt, grid(
          columns: 3,
          gutter: 1em,
          ..authors.map(author => align(center, author)),
        )),        
      ),
      align(right)[
        #image(height: 10cm, "dhbw-logo.svg", alt: "Logo of the Duale Hochschule Baden-Wuerttemberg Ravensburg")
      ],
      align(right)[
        #image(height: 10cm, seesat_logo, alt: "Logo of SeeSat e.V.")
      ],
    )
  )

  set par(justify: true)
  show heading: set text(fill: fg1)
  show figure: it => block(width: 100%)[#align(center)[
     #it.body
     #set text(size: 20pt)
     #it.caption
 ]]
  
  block(
    inset: (x: 2cm, bottom: 2cm),
    columns(
      2,
      gutter: 2cm,
      body
    ),
  )
}

#let contact(
) = {
  align(center)[
    #rect(
    stroke: 5pt + accent0,
    inset: 25pt,
    fill: backgroud,
    width: 75%,
    radius: 20pt)[
      #set text(fill: accent1)
      *SeeSat e.V.*\
      #set text(fill: black)
      Fallenbrunnen 14, 88045 Friedrichshafen, Germany\
      http://www.seesat.eu\
      info\@seesat.eu
    ]
  ]
}

#let references(
) = {
  set text(size: 24pt)
  align(right)[Background image from @BackgroundImage.]
  bibliography(
    style: "ieee",
    title: [References],
    "../refs.bib",
  )
  contact()
}