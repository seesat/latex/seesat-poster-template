#import "template/template.typ": *

#show: poster.with(
  title: "SeeSat Poster Template",
  authors: ("Max Muster", "Vera Vorlage"),
)

= Lorem ipsum dolor
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et orci justo. Duis ullamcorper sagittis felis quis pretium. Sed eleifend lobortis tortor nec maximus. Phasellus velit risus, porta eget ornare sit amet, viverra sit amet nibh. Maecenas sed commodo risus. Integer scelerisque velit sed urna imperdiet ornare. Aliquam nec ornare velit. Nulla quam quam, vestibulum volutpat lorem vel, facilisis consectetur tortor. Cras metus enim, sagittis ac posuere a, fermentum quis quam. Sed finibus suscipit augue. Morbi gravida enim augue, at pharetra tortor consectetur in. Nam in tortor et dui consectetur luctus. Nam aliquet erat id placerat iaculis. Pellentesque et sollicitudin velit, vitae auctor turpis. Vivamus cursus, est a aliquam sollicitudin, tellus quam facilisis est, quis dapibus ex justo eu leo. Nulla laoreet egestas ipsum id euismod.

Cras molestie massa sit amet nisi bibendum pellentesque. Pellentesque rhoncus lacus nibh, id ornare dui fringilla ut. Sed non mauris nunc. Aliquam erat volutpat. Curabitur bibendum, velit a fringilla pellentesque, libero enim tempus leo, non malesuada nisl nisl non augue. Sed euismod mattis lacus, nec auctor neque pulvinar sed. Aenean hendrerit lectus a sagittis maximus. Cras efficitur mi id massa luctus, id lacinia est tincidunt. Ut ac dolor velit. Nulla mollis nibh at tempor tincidunt.

#align(center)[
  #figure(
    image(
      "images/example.jpg", 
      height: 10%,
      alt: "SeeSat-1 Structure Layout"
    ),
    caption: [SeeSat-1 Structure Layout @ExampleImage]
  )
]

= Cras molestie massa sit
#lorem(50)

#lorem(150)

= Nulla ipsum elit
#lorem(40)

#lorem(80)

#lorem(90)

= Cras ullamcorper
#lorem(100)

#grid(columns: (50%, 50%),
      gutter: 1cm,
      figure(
        image(
          "images/example.jpg", 
          height: 10%, 
          alt: "SeeSat-1 Structure Layout"
        ),
        caption: [SeeSat-1 Structure Layout @ExampleImage]
      ),
      figure(
        image(
          "images/example.jpg", 
          height: 10%, 
          alt: "SeeSat-1 Structure Layout"
        ),
        caption: [SeeSat-1 Structure Layout @ExampleImage]
      )
)

#lorem(50)

= Donec pretium semper neque
#lorem(80)

#lorem(40)

#lorem(70)

#show: references()
